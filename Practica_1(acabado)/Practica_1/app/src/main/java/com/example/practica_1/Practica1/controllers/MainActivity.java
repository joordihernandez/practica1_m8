package com.example.practica_1.Practica1.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.practica_1.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLayoutMain);
        constraintLayout.setBackgroundColor(Color.rgb(125,125,125));
        Button btnBMI = findViewById(R.id.btnBMI);
        Button btnEuro = findViewById(R.id.btnEuro);
        btnBMI.setOnClickListener(this);
        btnEuro.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (v.getId()==R.id.btnBMI){
            Intent intentBMI = new Intent (this, CalculadoraBMI.class);
            startActivity(intentBMI);
        }
        if(v.getId() == R.id.btnEuro){
            Intent intentEuro = new Intent (this, CalculadoraEuroDolar.class);
            startActivity(intentEuro);
        }
    }
}

