package com.example.practica_1.Practica1.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica_1.Practica1.utils.MyUtils;
import com.example.practica_1.R;

public class CalculadoraBMI extends AppCompatActivity implements View.OnClickListener {

    EditText etAltura;
    EditText etkilos;
    Button btnCalcular;
    TextView tvCalculat;
    TextView tvResult2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_bmi);
        ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLayoutBMI);
        constraintLayout.setBackgroundColor(Color.rgb(125,125,125));
        etAltura = findViewById(R.id.etAltura);
        etkilos = findViewById(R.id.etKilos);
        btnCalcular = findViewById(R.id.btnCalcular);
        tvCalculat = findViewById(R.id.tvCalculat);
        btnCalcular.setOnClickListener(this);
        tvResult2 = findViewById(R.id.tvresult2);
    }

    @Override
    public void onClick(View v) {

        if (v.getId()==R.id.btnCalcular) {
            if (!MyUtils.editTextIsNullOrEmpty(etAltura)
                    && !MyUtils.editTextIsNullOrEmpty(etkilos)) {

                MyUtils.BMI(etAltura,etkilos,tvCalculat,tvResult2);

        }else{
                if (MyUtils.editTextIsNullOrEmpty(etAltura)) {
                    Toast.makeText(this, R.string.falta_altura, Toast.LENGTH_SHORT).show();
                    etAltura.requestFocus();

                }

                if (MyUtils.editTextIsNullOrEmpty(etkilos)) {
                    Toast.makeText(this,R.string.falta_pes , Toast.LENGTH_SHORT).show();
                    etkilos.requestFocus();

                }



            }
        }
    }
}
