package com.example.practica_1.Practica1.controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.example.practica_1.Practica1.utils.MyUtils;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica_1.R;

public class CalculadoraEuroDolar extends AppCompatActivity implements View.OnClickListener {

    EditText etCantidad;
    Button btnResult;
    TextView tvResult;
    RadioButton rdEuro;
    RadioButton rdDolar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_euro_dolar);
        ConstraintLayout constraintLayout = findViewById(R.id.ConstraintLayoutEuro);
        constraintLayout.setBackgroundColor(Color.rgb(125,125,125));
        etCantidad = findViewById(R.id.etCantidad);
        btnResult = findViewById(R.id.btnResult);
        tvResult = findViewById(R.id.tvResult);
        btnResult.setOnClickListener(this);
        rdEuro = findViewById(R.id.rdEuro);
        rdDolar = findViewById(R.id.rdDolar);
        rdEuro.setChecked(true);
        rdEuro.setOnClickListener(this);
        rdDolar.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (v == rdEuro){
            rdDolar.setChecked(false);
        }

        if (v == rdDolar){
            rdEuro.setChecked(false);
        }
        if (v.getId()==R.id.btnResult){

            if (!MyUtils.editTextIsNullOrEmpty(etCantidad)) {
                if (rdEuro.isChecked()) {
                    MyUtils.convertirEuroDolar(etCantidad,tvResult);
                }
                if (rdDolar.isChecked()) {
                    MyUtils.convertirDolarEuro(etCantidad,tvResult);
                }
        }else{
                if (MyUtils.editTextIsNullOrEmpty(etCantidad)) {
                    Toast.makeText(this, R.string.falta_quantitat, Toast.LENGTH_SHORT).show();
                    etCantidad.requestFocus();

                }
            }
    }
}
}
