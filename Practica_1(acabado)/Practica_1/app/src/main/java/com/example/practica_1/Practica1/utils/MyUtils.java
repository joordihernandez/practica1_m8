package com.example.practica_1.Practica1.utils;

import android.graphics.Color;
import android.widget.EditText;
import android.widget.TextView;

import com.example.practica_1.R;

import java.text.DecimalFormat;

public class MyUtils {

    public static boolean stringIsNullOrEmpty (String cadena)

    {
        return (cadena == null ||cadena.length()==0);

    }

    public static boolean editTextIsNullOrEmpty (EditText editText)
    {
        return stringIsNullOrEmpty (editText.getText().toString());
    }

    public static void convertirEuroDolar(EditText recollir, TextView sortida){
        DecimalFormat formato = new DecimalFormat("0.00");
        double val;
        if ("".equals(recollir.getText().toString())){
            val = 0;
        }else{
            val = Double.parseDouble(recollir.getText().toString());
        }
        sortida.setText(formato.format(val*1.0831)+" Dolars");
        sortida.setTextColor(Color.rgb(94,0,100));
    }

    public static void convertirDolarEuro(EditText recollir, TextView sortida){
        DecimalFormat formato = new DecimalFormat("0.00");
        double val;
        if ("".equals(recollir.getText().toString())){
            val = 0;
        }else{


            val = Double.parseDouble(recollir.getText().toString());
        }
        sortida.setText(formato.format(val* 0.9233)+" Euros");
        sortida.setTextColor(Color.BLUE);
    }


    public static void BMI(EditText cm, EditText kg,TextView result,TextView result1) {
        DecimalFormat formato = new DecimalFormat("0.00");
        float metros = Float.parseFloat((cm.getText().toString()))/100;
        float peso = Float.parseFloat(kg.getText().toString());
        float result3 = peso/(metros*metros);
        if (Float.compare(result3, 15f) <= 0) {
            result.setText(R.string.molt_baix_de_pes);
            result.setTextColor(Color.RED);
            result1.setTextColor(Color.RED);
        } else if (Float.compare(result3, 15f) > 0  &&  Float.compare(result3, 16f) <= 0) {
            result.setText(R.string.severament_baix_de_pes);
            result.setTextColor(Color.YELLOW);
            result1.setTextColor(Color.YELLOW);
        } else if (Float.compare(result3, 16f) > 0  &&  Float.compare(result3, 18.5f) <= 0) {
            result.setText(R.string.baix_de_pes);
            result.setTextColor(Color.rgb(255,128,0));
            result1.setTextColor(Color.rgb(255,128,0));
        } else if (Float.compare(result3, 18.5f) > 0  &&  Float.compare(result3, 25f) <= 0) {
            result.setText(R.string.normal);
            result.setTextColor(Color.rgb(45,87,44));
            result1.setTextColor(Color.rgb(45,87,44));
        } else if (Float.compare(result3, 25f) > 0  &&  Float.compare(result3, 30f) <= 0) {
            result.setText(R.string.sobrepes);
            result.setTextColor(Color.YELLOW);
            result1.setTextColor(Color.YELLOW);
        } else if (Float.compare(result3, 30f) > 0  &&  Float.compare(result3, 35f) <= 0) {
            result.setText(R.string.classe1);
            result.setTextColor(Color.rgb(255,128,0));
            result1.setTextColor(Color.rgb(255,128,0));
        } else if (Float.compare(result3, 35f) > 0  &&  Float.compare(result3, 40f) <= 0) {
            result.setText(R.string.classe2);
            result.setTextColor(Color.RED);
            result1.setTextColor(Color.RED);
        } else {
            result.setText(R.string.classe3);
            result.setTextColor(Color.RED);
            result1.setTextColor(Color.RED);
        }

        result1.setText(String.valueOf(formato.format(result3)));
    }




}
